import { h, render, FunctionalComponent } from 'preact'

const App: FunctionalComponent = () => <p>Hi, Preact!</p>

render(<App />, document.body)
