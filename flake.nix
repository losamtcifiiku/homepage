{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    napalm.url = "github:nmattia/napalm";

    napalm.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, napalm, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (pkgs) stdenv lib;
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            git
            nodejs-14_x
            nodePackages.node2nix
            nodePackages.pnpm
          ];
        };

        packages = {
          nodeModules = (pkgs.callPackage ./. {
            nodejs = pkgs.nodejs-14_x;
          }).nodeDependencies;
          losamtcifiiku = stdenv.mkDerivation {
            pname = "losamtcifiiku";
            version = "1.0.0";
            src = ./.;

            buildPhase = ''
              ln -s ${packages.nodeModules}/lib/node_modules
              export PATH="${packages.nodeModules}/bin:$PATH"
              parcel build src/index.html
            '';

            installPhase = ''
              mkdir $out
              cp -r dist/. $out
            '';
          };
        };

        defaultPackage = packages.losamtcifiiku;
      });
}
